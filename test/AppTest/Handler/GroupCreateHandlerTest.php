<?php

declare(strict_types=1);

namespace AppTest\Handler;

use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Handler\GroupCreateHandler;
use App\Models\GroupsModel;
use App\Models\UsersModel;
use App\Models\UsersGroupsModel;
use App\Handler\Mail\Transport;
use Prophecy\Argument;
use Zend\Diactoros\Response\HtmlResponse;

class GroupCreateHandlerTest extends TestCase {

    /** @var ContainerInterface|ObjectProphecy */
    protected $container;

    /** @var RouterInterface|ObjectProphecy */
    protected $router;

    /** @var TemplateRendererInterface|ObjectProphecy */
    protected $renderer;

    protected function setUp()
    {
        $this->container = $this->prophesize(ContainerInterface::class);
        $this->router    = $this->prophesize(RouterInterface::class);
        $this->renderer = $this->prophesize(TemplateRendererInterface::class);
    }

    public function testReturnsHtmlResponseWhenTemplateRendererProvided(){
        $this->renderer
            ->render('app::group-create', Argument::type('array'))
            ->willReturn('');

        $groupCreate = new GroupCreateHandler($this->renderer->reveal(),
            $this->router->reveal(),
            $this->prophesize(GroupsModel::class)->reveal(),
            $this->prophesize(UsersModel::class)->reveal(),
            $this->prophesize(UsersGroupsModel::class)->reveal(),
            $this->prophesize(Transport::class)->reveal()
        );

        $request = $this->prophesize(ServerRequestInterface::class);
        $request->getMethod()->willReturn('GET');

        $response = $groupCreate->handle($request->reveal());
        $this->assertInstanceOf(HtmlResponse::class, $response);
    }

}