<?php

declare(strict_types = 1);

use App\Handler\GroupAddMemberHandler;
use App\Handler\GroupViewHandler;
use App\Handler\HomePageHandler;
use App\Handler\PingHandler;
use Psr\Container\ContainerInterface;
use Zend\Expressive\Application;
use Zend\Expressive\MiddlewareFactory;
use App\Handler\GroupCreateHandler;
use App\Handler\LogginHandler;

/**
 * Setup routes with a single request method:
 *
 * $app->get('/', App\Handler\HomePageHandler::class, 'home');
 * $app->post('/album', App\Handler\AlbumCreateHandler::class, 'album.create');
 * $app->put('/album/:id', App\Handler\AlbumUpdateHandler::class, 'album.put');
 * $app->patch('/album/:id', App\Handler\AlbumUpdateHandler::class, 'album.patch');
 * $app->delete('/album/:id', App\Handler\AlbumDeleteHandler::class, 'album.delete');
 *
 * Or with multiple request methods:
 *
 * $app->route('/contact', App\Handler\ContactHandler::class, ['GET', 'POST', ...], 'contact');
 *
 * Or handling all request methods:
 *
 * $app->route('/contact', App\Handler\ContactHandler::class)->setName('contact');
 *
 * or:
 *
 * $app->route(
 *     '/contact',
 *     App\Handler\ContactHandler::class,
 *     Zend\Expressive\Router\Route::HTTP_METHOD_ANY,
 *     'contact'
 * );
 */
return function (Application $app, MiddlewareFactory $factory, ContainerInterface $container): void {
    $app->get('/', HomePageHandler::class, 'home');
    $app->get('/group/{id}/', GroupViewHandler::class, 'group.view');
    $app->route('/group/view/{name}/{key}', GroupViewHandler::class, ['GET', 'POST'] ,'group.view');
    $app->get('/group/create', GroupCreateHandler::class, 'group.create');
    $app->post('/group/create', GroupCreateHandler::class, 'group.create.post');
    $app->get('/loggin',LogginHandler::class, 'loggin');
//    $app->post('/group', App\Handler\GroupCreateHandler::class, 'group.create');
    $app->get('/api/ping', PingHandler::class, 'api.ping');

};
