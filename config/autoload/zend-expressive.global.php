<?php

declare(strict_types = 1);

use Zend\ConfigAggregator\ConfigAggregator;

return [
    // Toggle the configuration cache. Set this to boolean false, or remove the
    // directive, to disable configuration caching. Toggling development mode
    // will also disable it by default; clear the configuration cache using
    // `composer clear-config-cache`.
    ConfigAggregator::ENABLE_CACHE => true,
    // Enable debugging; typically used to provide debugging information within templates.
    'debug' => false,
    'zend-expressive' => [
        // Provide templates for the error handling middleware to use when
        // generating responses.
        'error_handler' => [
            'template_404' => 'error::404',
            'template_error' => 'error::error',
        ],
    ],
    'translator' => [
        'locale' => 'fr_FR',
        'translation_file_patterns' => [
            [
                'type' => 'phparray',
                'base_dir' => __DIR__ . '/../../data/languages',
                'pattern' => '%s.php',
            ],
        ],
    ],
    'twig' => [
        'extensions' => [
            // extension service names or instances
            App\Twig\TwigExtension::class
        ],
    ],
    'console' => [
        'commands' => [
            App\Command\UpdateDateAction::class,
        ],
    ]
];
