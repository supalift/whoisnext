<?php

declare(strict_types = 1);

namespace App\Twig;

use Twig_ExtensionInterface as TwigExtensionInterface;
use Twig_SimpleFilter;
use Zend\I18n\Translator\Translator;

class TwigExtension implements TwigExtensionInterface {

    private $trans;
    
    public function __construct(Translator $translator) {
        $this->trans = $translator;
    }
    
    public function getFilters() {
        return [
            new Twig_SimpleFilter('trans', function($text) {
                echo $this->trans->translate($text);
            }),
        ];

    }

    public function getFunctions() {
        return [];

    }

    public function getNodeVisitors() {
        return [];

    }

    public function getOperators() {
        return [];

    }

    public function getTests() {
        return [];

    }

    public function getTokenParsers() {
        return [];

    }

}
