<?php

declare(strict_types=1);

namespace App\Twig;

use App\Twig\TwigExtension;
use Psr\Container\ContainerInterface;
use Zend\I18n\Translator\TranslatorInterface;

class TwigExtensionFactory{

    public function __invoke(ContainerInterface $container) : TwigExtension
    {
        return new TwigExtension($container->get(TranslatorInterface::class));
    }
}