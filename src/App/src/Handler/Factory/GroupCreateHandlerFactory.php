<?php
declare (strict_types = 1);

namespace App\Handler\Factory;

use App\Models\GroupsModel;
use App\Handler\GroupCreateHandler;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use App\Models\UsersModel;
use App\Models\UsersGroupsModel;
use Zend\Expressive\Router\RouterInterface;
use App\Handler\Mail\Transport;

class GroupCreateHandlerFactory
{
    public function __invoke(ContainerInterface $container): RequestHandlerInterface
    {
        $template = $container->has(TemplateRendererInterface::class)
            ? $container->get(TemplateRendererInterface::class)
            : null;

        $groupsModel = $container->get(GroupsModel::class);
        $usersModel = $container->get(UsersModel::class);
        $UsersGroupsModel = $container->get(UsersGroupsModel::class);
        $router   = $container->get(RouterInterface::class);
        $mail = $container->get(Transport::class);


        return new GroupCreateHandler($template, $router, $groupsModel, $usersModel, $UsersGroupsModel, $mail);
    }
}
