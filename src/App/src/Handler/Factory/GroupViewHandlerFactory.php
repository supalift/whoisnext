<?php
declare (strict_types = 1);

namespace App\Handler\Factory;

use App\Models\UsersModel;
use App\Models\UsersGroupsModel;
use App\Handler\GroupViewHandler;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use App\Models\GroupsModel;

class GroupViewHandlerFactory
{
    public function __invoke(ContainerInterface $container): RequestHandlerInterface
    {
        $template = $container->has(TemplateRendererInterface::class)
            ? $container->get(TemplateRendererInterface::class)
            : null;

        $modelUsers = $container->get(UsersModel::class);
        $modelUsersGroups = $container->get(UsersGroupsModel::class);
        $groupsModel = $container->get(GroupsModel::class);

        return new GroupViewHandler($template, $modelUsers, $modelUsersGroups, $groupsModel);
    }
}
