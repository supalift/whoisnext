<?php

declare (strict_types = 1);

namespace App\Handler\Factory;

use App\Handler\LogginHandler;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class LogginHandlerFactory
{
    public function __invoke(ContainerInterface $container): RequestHandlerInterface
    {

        $template = $container->has(TemplateRendererInterface::class)
            ? $container->get(TemplateRendererInterface::class)
            : null;

        return new LogginHandler($template);
    }
}
