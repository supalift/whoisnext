<?php
declare(strict_types=1);

namespace App\Handler\Factory;

use App\Handler\GroupSearchHandler;
use App\Models\GroupsModel;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class GroupSearchHandlerFactory
{
    public function __invoke(ContainerInterface $container) : RequestHandlerInterface
    {
        $template = $container->has(TemplateRendererInterface::class)
            ? $container->get(TemplateRendererInterface::class)
            : null;
        
        $modelGroups = $container->get(GroupsModel::class);
        
        return new GroupSearchHandler($template, $modelGroups);
    }
}
