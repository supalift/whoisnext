<?php
declare (strict_types = 1);

namespace App\Handler;

use Datetime;
use App\Constantes\Format;
use App\Models\UsersModel;
use App\Models\GroupsModel;
use App\Entities\UsersEntity;
use App\Entities\GroupsEntity;
use App\Forms\GroupCreateForm;
use App\Models\UsersGroupsModel;
use App\Entities\UsersGroupsEntity;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;
use App\Handler\Mail\Transport;

class GroupCreateHandler implements RequestHandlerInterface
{

    private $template;
    private $modelGroup;
    private $modelUsers;
    private $modelUsersGroups;
    private $router;
    private $dataView;
    private $mail;

    public function __construct(TemplateRendererInterface $template = null, RouterInterface $router, GroupsModel $modelGroup, UsersModel $modelUsers, UsersGroupsModel $modelUsersGroups, Transport $mail)
    {
        $this->template = $template;
        $this->modelGroup = $modelGroup;
        $this->modelUsers = $modelUsers;
        $this->router = $router;
        $this->modelUsersGroups = $modelUsersGroups;
        $this->mail = $mail;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->dataView = [];
        if ($request->getMethod() === "POST") {
            $this->postCreateGroup($request);
        }

        return new HtmlResponse($this->template->render('app::group-create', $this->dataView));
    }


    private function postCreateGroup(ServerRequestInterface $request)
    {
        $formGroupCreate = new GroupCreateForm();
        $formGroupCreate->setData($request->getParsedBody());

        if ($formGroupCreate->isValid()) {
            $groupEntity = $this->createGroup($formGroupCreate->getGroupEntity(), $formGroupCreate->getUserEntity());
            
            return new RedirectResponse($this->router->generateUri('group.view',['id' => $groupEntity->getId()]));
        } else {
            $this->dataView = $formGroupCreate->getErrorMessage();
        }
    }

    private function sendMail(){
        $mail = new Message();
        $mail->setBody('mon bodi')
        ->setFrom('mail@whoisit.com')
        ->addTo('tets@test.com','nom')
        ->setSubject('test');

        $this->mail->getSmtp()->send($mail);
    }

    private function createGroup(GroupsEntity $groupEntity, UsersEntity $userEntity) : GroupsEntity
    {

        $usersRows = $this->modelUsers->find(['email' => $userEntity->getEmail()]);;

        if ($usersRows->count() === 1) {
            $userEntity = $usersRows->current();
        } else {
            $userEntity = $this->modelUsers->add($userEntity);
        }

        $groupEntity->setId_user($userEntity->getId())
            ->setPeriod('next '.$groupEntity->getPeriod());
        $groupEntity = $this->modelGroup->add($groupEntity);
        
        //set url_custom
        $keyGroup = base64_encode($groupEntity.getId().'|'.$groupEntity->getName());

        $groupEntity->setUrl_custom($this->router->generateUri('group.view.custom',
            ['name' => $groupEntity->getName(),
            'key' => $keyGroup]));
        $groupEntity = $this->modelGroup->add($groupEntity);

        $dateToAction = new DateTime();
        $dateToAction->modify($groupEntity->getPeriod());

        $usersGroupsEntity = new UsersGroupsEntity();
        $usersGroupsEntity->setId_group($groupEntity->getId())
            ->setId_user($userEntity->getId())
            ->setDate_action($dateToAction->format(Format::DATE_DB));

        $this->modelUsersGroups->add($usersGroupsEntity);

        return $groupEntity;
    }
}
