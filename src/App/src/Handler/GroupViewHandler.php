<?php
declare (strict_types = 1);

namespace App\Handler;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template\TemplateRendererInterface;
use App\Models\UsersModel;
use App\Models\UsersGroupsModel;
use App\Models\GroupsModel;
use App\Entities\UsersGroupsEntity;
use App\Entities\UsersEntity;
use DateTime;
use App\Constantes\Format;
use App\Forms\GroupAddMemberForm;
use App\Entities\GroupsEntity;
use App\Handler\ViewMessage\ViewMessage;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Mvc\Plugin\FlashMessenger\FlashMessenger;

class GroupViewHandler implements RequestHandlerInterface
{

    private $template;
    /** @var \App\Models\users modelUsers **/
    private $modelUsers;
    private $modelUsersGroups;
    private $modelGroups;

    private $viewMessage;

    public function __construct(TemplateRendererInterface $template = null, UsersModel $modelUsers, UsersGroupsModel $modelUsersGroups, GroupsModel $groupsModel)
    {

        $this->template = $template;
        $this->modelUsers = $modelUsers;
        $this->modelUsersGroups = $modelUsersGroups;
        $this->modelGroups = $groupsModel;

        $this->viewMessage = new ViewMessage();
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $isOk = true;

        //custom view
        $name = $request->getAttribute('name', null);
        if ($name) {
            $arrayKey = explode('|', \base64_decode($request->getAttribute('key', null)), 2);
            $idGroup = (int)$arrayKey[0];
            $isOk = $name == $arrayKey[1];
        } else {
            $idGroup = (int)$request->getAttribute('id', 0);
            $isOk = $idGroup > 0;
        }

        if ($isOk) {

            $flashMsg = new FlashMessenger();
            $this->viewMessage->addSuccessMessages($flashMsg->getCurrentSuccessMessages());

            $group = $this->modelGroups->findOne(['id' => $idGroup]);

            if ($request->getMethod() === "POST") {
                if($this->addUserGroup($request, $group)){
                    $flashMsg->addSuccessMessage('USER_ADD_SUCCEEDS_IN_THIS_GROUP');

                    return new RedirectResponse($name);
                }
            }

            $members = $this->modelUsersGroups->getNextMembersGroup($idGroup);

            $data = [
                'members' => $members,
                'group' => $group,
                'formData' => [],
                'formMessages' => []
            ];

            $data['viewMessages'] = $this->viewMessage->getViewMessages();

            return new HtmlResponse($this->template->render('app::group-view', $data));
        } else {
            return new HtmlResponse($this->template->render('error::404'));
        }
    }

    private function addUserGroup(ServerRequestInterface $request,GroupsEntity $group)
    {
        $form = new GroupAddMemberForm();
        $form->setData($request->getParsedBody());

        if ($form->isValid()) {

            $userEntity = $this->modelUsers->findOne(['email' => $form->get('email')->getValue()]);
            if (!$userEntity) {
                $userEntity = new UsersEntity();
                $userEntity->exchangeArray($form->getData());
                $userEntity->setCreated(date(Format::DATE_DB));
                $userEntity = $this->modelUsers->add($userEntity);
            }

            $userGroupEntity = $this->modelUsersGroups->findOne([
                'id_user' => $userEntity->getId(),
                'id_group' => $group->getId()
            ]);

            if (!$userGroupEntity) {
                //Ajout d'un nouveau membre au groupe
                $lastMember = $this->modelUsersGroups->getLastToPass($group->getId());
                if ($lastMember) {
                    $dateToAction = new DateTime($lastMember->getDate_action());
                } else {
                    $dateToAction = new DateTime();
                }

                //La nouvelle date sera le prochain passage du dernier à passer
                $dateToAction->modify($group->getPeriod());
                
                $userGroupEntity = new UsersGroupsEntity();
                $userGroupEntity->setId_group($group->getId())
                ->setId_user($userEntity->getId())
                ->setDate_action($dateToAction->format(Format::DATE_DB));

                $userGroupEntity = $this->modelUsersGroups->add($userGroupEntity);

                $this->viewMessage->addSuccessMessage("USER_ADD_SUCCEEDS_IN_THIS_GROUP");

            } else {
                $this->viewMessage->addErrorMessage('USER_ALREADY_IN_THIS_GROUP');
            }
        } else {
            $this->dataView['formMessages'] = $form->getMessages();
            $this->dataView['formData'] = $form->getData();
        }
    }
}
