<?php

declare(strict_types=1);

namespace App\Handler\ViewMessage;

class ViewMessage {

    private $viewMessages;

    public function __construct()
    {
        $this->viewMessages = [
            'viewMessage' => [
                'success' => [],
                'error' => []
            ],
            'form' => [
                'messages' => [],
                'value' => []
            ]
        ];
    }

    public function addSuccessMessage(string $message){
        $this->viewMessages['viewMessage']['success'][] = $message;
    }

    public function addSuccessMessages(array $messages){
        $this->viewMessages['viewMessage']['success'] = array_merge($this->viewMessages['viewMessage']['success'], $messages);
    }

    public function addErrorMessage(string $message){
        $this->viewMessages['viewMessage']['error'][] = $message;
    }

    public function getViewMessages () :array{
        return $this->viewMessages;
    }
}