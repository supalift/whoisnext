<?php
declare (strict_types = 1);

namespace App\Handler\Mail;

use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 *
 * @author lifting
 */
class Transport {    

    private $configTransport;

    public function __construct(array $configTransport) {        
        $this->configTransport = $configTransport;
    }

    public function getSmtp() : Smtp{
        $transport = new Smtp();
        $options = new SmtpOptions($this->configTransport);
        $transport->setOptions($options);

        return $transport;
    }

}
