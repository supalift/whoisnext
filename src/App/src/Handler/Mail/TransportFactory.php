<?php
declare (strict_types = 1);

namespace App\Handler\Mail;

use Psr\Container\ContainerInterface;

class TransportFactory
{
    public function __invoke(ContainerInterface $container): Transport
    {
        $mailConfig = $container->get('config')['mail'];

        return new Transport($mailConfig['transport']);
    }
}
