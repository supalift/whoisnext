<?php

declare(strict_types=1);

namespace App\Handler;

use App\Forms\GroupSearchForm;
use App\Models\GroupsModel;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template\TemplateRendererInterface;

class GroupSearchHandler implements RequestHandlerInterface {

    private $template;
    private $modelGroups;
    private $dataView;

    public function __construct(TemplateRendererInterface $template = null, GroupsModel $modelGroups) {

        $this->template = $template;
        $this->modelGroups = $modelGroups;

    }

    public function handle(ServerRequestInterface $request): ResponseInterface {

        if ($request->getMethod() === "POST") {
            $form = new GroupSearchForm();
            $form->setData($request->getParsedBody());

            if ($form->isValid()) {
                $this->dataView['resultSearch'] = $this->modelGroups->find(array('name LIKE ?' => '%'.$form->get('name')->getValue().'%'))->toArray();
            } else {
                $this->dataView['formMessages'] = $form->getMessages();
                $this->dataView['formData'] = $form->getData();
            }
        }

        return new HtmlResponse($this->template->render('app::group-search', $this->dataView));

    }

}
