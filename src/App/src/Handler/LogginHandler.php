<?php

declare(strict_types = 1);

namespace App\Handler;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template;

class LogginHandler implements RequestHandlerInterface {

    private $containerName;
    private $router;
    private $template;
    /*     * @var \App\Models\users modelUsers * */
    private $modelUsers;

    public function __construct(Template\TemplateRendererInterface $template = null) {
        $this->template = $template;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface {

        return new HtmlResponse($this->template->render('app::loggin',[]));
    }

}
