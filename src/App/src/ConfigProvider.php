<?php

declare(strict_types = 1);

namespace App;

use App\Models\UsersModel;
use App\Models\GroupsModel;
use App\Twig\TwigExtension;
use App\Handler\PingHandler;
use App\Handler\LogginHandler;
use App\Handler\Mail\Transport;
use App\Handler\HomePageHandler;
use App\Models\UsersGroupsModel;
use App\Handler\GroupViewHandler;
use App\Twig\TwigExtensionFactory;
use App\Handler\GroupCreateHandler;
use App\Handler\Mail\TransportFactory;
use App\Models\Factories\UsersModelFactory;
use App\Models\Factories\GroupsModelFactory;
use App\Handler\Factory\LogginHandlerFactory;
use App\Handler\Factory\HomePageHandlerFactory;
use App\Handler\Factory\GroupViewHandlerFactory;
use App\Models\Factories\UsersGroupsModelFactory;
use App\Handler\Factory\GroupCreateHandlerFactory;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider {

    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     */
    public function __invoke(): array {
        return [
            'dependencies' => $this->getDependencies(),
            'templates' => $this->getTemplates(),
        ];

    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies(): array {
        return [
            'invokables' => [
                PingHandler::class => PingHandler::class,
            ],
            'factories' => [
                HomePageHandler::class => HomePageHandlerFactory::class,
                GroupViewHandler::class => GroupViewHandlerFactory::class,
                GroupCreateHandler::class => GroupCreateHandlerFactory::class,
                Transport::class => TransportFactory::class,
                LogginHandler::class => LogginHandlerFactory::class,                
                TwigExtension::class => TwigExtensionFactory::class,
                UsersModel::class => UsersModelFactory::class,
                GroupsModel::class => GroupsModelFactory::class,
                UsersGroupsModel::class => UsersGroupsModelFactory::class,
            ],
        ];

    }

    /**
     * Returns the templates configuration
     */
    public function getTemplates(): array {
        return [
            'paths' => [
                'app' => [__DIR__ . '/../templates/app'],
                'error' => [__DIR__ . '/../templates/error'],
                'layout' => [__DIR__ . '/../templates/layout'],
                'macros' => [__DIR__ . '/../templates/macros'],
            ],
        ];

    }

}
