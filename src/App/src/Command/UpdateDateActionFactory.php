<?php
declare (strict_types = 1);

namespace App\Command;

use Interop\Container\ContainerInterface;
use App\Models\UsersGroupsModel;

class UpdateDateActionFactory{

    public function __invoke(ContainerInterface $container)
    {
        $modelUsersGroups = $container->get(UsersGroupsModel::class);

        return new UpdateDateAction($modelUsersGroups);
    }

}