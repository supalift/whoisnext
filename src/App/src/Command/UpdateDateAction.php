<?php
declare (strict_types = 1);

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Models\UsersGroupsModel;

class UpdateDateAction extends Command {

    private $userGroupModel;

    public function __construct( UsersGroupsModel $modelUsersGroups)
    {
        $this->userGroupModel = $modelUsersGroups;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('updateDateAction')
        ->setDescription('update date action for all group');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $nb = $this->userGroupModel->updateMemberForNextDate();
        $output->writeln('Nb member : '.$nb);
    }

}
