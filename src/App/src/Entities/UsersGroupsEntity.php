<?php

namespace App\Entities;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 *
 * @author lifting
 */
class UsersGroupsEntity implements Entity {

    private $id;
    private $id_user;
    private $id_group;
    private $date_action;

    public function exchangeArray(array $data) : Entity{

        if (isset($data['id'])) {
            $this->id = $data['id'];
        }
        if (isset($data['id_user'])) {
            $this->id_user = $data['id_user'];
        }
        if (isset($data['id_group'])) {
            $this->id_group = $data['id_group'];
        }
        if (isset($data['date_action'])) {
            $this->date_action = $data['date_action'];
        }
        return $this;
    }
    
    public function toArray(): array {
        $data['id'] = $this->id;
        $data['id_group'] = $this->id_group;
        $data['id_user'] = $this->id_user;
        $data['date_action'] = $this->date_action;

        return $data;
    }

    function getId() {
        return $this->id;
    }

    function getId_user() {
        return $this->id_user;
    }

    function getId_group() {
        return $this->id_group;
    }

    function setId($id) {
        $this->id = $id;
        return $this;
    }

    function setId_user($id_user) {
        $this->id_user = $id_user;
        return $this;
    }

    function setId_group($id_group) {
        $this->id_group = $id_group;
        return $this;
    }

    function getDate_action() {
        return $this->date_action;
    }

    function setDate_action($date_action) {
        $this->date_action = $date_action;
        return $this;
    }

}
