<?php

namespace App\Entities;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 *
 * @author lifting
 */
class UsersEntity implements Entity {

    private $id;
    private $email;
    private $name;
    private $created;
    
    public function exchangeArray(array $data) : Entity{

        if (isset($data['email'])) {
            $this->email = $data['email'];
        }
        if (isset($data['id'])) {
            $this->id = $data['id'];
        }
        if (isset($data['name'])) {
            $this->name = $data['name'];
        }
        if (isset($data['created'])) {
            $this->created = $data['created'];
        }

        return $this;
    }

    public function toArray(): array {
        $data['email'] = $this->email;
        $data['id'] = $this->id;
        $data['name'] = $this->name;
        $data['created'] = $this->created;

        return $data;
    }

    function getEmail(): string {
        return $this->email;
    }

    function setEmail(string $email) {
        $this->email = $email;
        return $this;
    }

    function getId(): int {
        return $this->id;
    }

    function setId(int $id) {
        $this->id = $id;
        return $this;
    }


    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of created
     */ 
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set the value of created
     *
     * @return  self
     */ 
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }
}
