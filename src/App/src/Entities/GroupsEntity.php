<?php
declare (strict_types = 1);

namespace App\Entities;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 *
 * @author lifting
 */
class GroupsEntity implements Entity {

    private $id;
    private $name;
    private $period;
    private $id_user;
    private $url_custom;
    private $created;

    public function exchangeArray(array $data): Entity {
        if (isset($data['id'])) {
            $this->id = $data['id'];
        }

        if (isset($data['name'])) {
            $this->name = $data['name'];
        }

        if (isset($data['period'])) {
            $this->period = $data['period'];
        }

        if (isset($data['id_user'])) {
            $this->id_user = $data['id_user'];
        }

        if (isset($data['url_custom'])) {
            $this->url_custom = $data['url_custom'];
        }

        if (isset($data['created'])) {
            $this->created = $data['created'];
        }
        
        return $this;
    }

    public function toArray(): array {
        $data['id'] = $this->id;
        $data['name'] = $this->name;
        $data['period'] = $this->period;
        $data['id_user'] = $this->id_user;
        $data['url_custom'] = $this->url_custom;
        $data['created'] = $this->created;

        return $data;
    }
    
    function getName() {
        return $this->name;
    }

    function getId() {
        return (int) $this->id;
    }

    function getPeriod() {
        return $this->period;
    }

    function getId_user() {
        return (int) $this->id_user;
    }

    function setName($name) {
        $this->name = $name;
        return $this;
    }

    function setId($id) {
        $this->id = $id;
        return $this;
    }

    function setPeriod($period) {
        $this->period = $period;
        return $this;
    }

    function setId_user($id_user) {
        $this->id_user = $id_user;
        return $this;
    }

    /**
     * Get the value of url_custom
     */ 
    public function getUrl_custom()
    {
        return $this->url_custom;
    }

    /**
     * Set the value of url_custom
     *
     * @return  self
     */ 
    public function setUrl_custom($url_custom)
    {
        $this->url_custom = $url_custom;

        return $this;
    }

    /**
     * Get the value of created
     */ 
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set the value of created
     *
     * @return  self
     */ 
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }
}
