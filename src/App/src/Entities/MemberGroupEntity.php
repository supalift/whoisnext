<?php

namespace App\Entities;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 *
 * @author lifting
 */
class MemberGroupEntity implements Entity {

    private $idUser;
    private $idGroup;
    private $name;
    private $dateAction;
    private $nb_passage;

    public function exchangeArray(array $data): Entity {
        if (isset($data['idUser'])) {
            $this->idUser = $data['idUser'];
        }

        if (isset($data['idGroup'])) {
            $this->idGroup = $data['idGroup'];
        }
        
        if (isset($data['name'])) {
            $this->name = $data['name'];
        }
        
        if (isset($data['dateAction'])) {
            $this->dateAction = $data['dateAction'];
        }

        if (isset($data['nb_passage'])) {
            $this->nb_passage = $data['nb_passage'];
        }
        
        return $this;
    }

    public function toArray(): array {
        $data['idUser'] = $this->idUser;
        $data['idGroup'] = $this->idGroup;
        $data['name'] = $this->name;
        $data['dateAction'] = $this->dateAction;
        $data['nb_passage'] = $this->nb_passage;

        return $data;
    }
  
    function getIdUser() {
        return $this->idUser;

    }

    function getIdGroup() {
        return $this->idGroup;

    }

    function getName() {
        return $this->name;

    }

    function getDateAction() {
        return $this->dateAction;

    }

    function setIdUser($idUser) {
        $this->idUser = $idUser;
        return $this;

    }

    function setIdGroup($idGroup) {
        $this->idGroup = $idGroup;
        return $this;

    }

    function setName($name) {
        $this->name = $name;
        return $this;

    }

    function setDateAction($dateAction) {
        $this->dateAction = $dateAction;
        return $this;

    }
  

    /**
     * Get the value of nb_passage
     */ 
    public function getNb_passage()
    {
        return $this->nb_passage;
    }

    /**
     * Set the value of nb_passage
     *
     * @return  self
     */ 
    public function setNb_passage($nb_passage)
    {
        $this->nb_passage = $nb_passage;

        return $this;
    }
}
