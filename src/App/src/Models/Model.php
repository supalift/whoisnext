<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use App\Entities\entity;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\RowGateway\RowGateway;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\RowGateway\AbstractRowGateway;

/**
 * Description of model
 *
 * @author lifting
 */
class Model {
    
    protected $entityClass;
    /**@var \Zend\Db\TableGateway\TableGateway */
    protected $tableGateway;
    
     public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }
    
    public function find(array $where) : ResultSet{
        $res = $this->tableGateway->select($where);
        return $res->setArrayObjectPrototype(new $this->entityClass());
    }
    
    public function findOne(array $where) : ?entity{
        $results = $this->find($where);
        if($results->count() > 0){
            return $results->current();
        }else{
            return null;
        }
    }
    
    public function fetchAll() {
        return $this->tableGateway->select();
    }
    
    public function add(entity $entity) : entity {        
        $row = $this->getRowGateway($entity);
        $row->save();        
        return $entity->exchangeArray($row->toArray());
    }
    
    private function getRowGateway(entity $entity) : AbstractRowGateway{
        $row = new RowGateway('id', $this->tableGateway->getTable(), $this->tableGateway->getSql());
        return $row->populate($entity->toArray());
    }
    
    public function queryString(string $sql, array $parameters = []): array{
        return $this->tableGateway->getAdapter()->query($sql)->execute($parameters)->getResource()->fetchAll(\PDO::FETCH_ASSOC);
    }
}
