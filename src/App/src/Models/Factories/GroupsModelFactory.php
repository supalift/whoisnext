<?php

declare (strict_types = 1);

namespace App\Models\Factories;

use App\Models\GroupsModel;
use Psr\Container\ContainerInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Zend\Db\Adapter\AdapterInterface;

class GroupsModelFactory
{

    public function __invoke(ContainerInterface $container): GroupsModel
    {
        $tableGateway = new TableGateway('groups', $container->get(AdapterInterface::class), new RowGatewayFeature('id'));
        return new GroupsModel($tableGateway);
    }
}

