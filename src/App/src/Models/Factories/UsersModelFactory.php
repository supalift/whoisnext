<?php

declare (strict_types = 1);

namespace App\Models\Factories;

use App\Models\UsersModel;
use Psr\Container\ContainerInterface;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;

class UsersModelFactory
{

    public function __invoke(ContainerInterface $container): UsersModel
    {
        $tableGateway = new TableGateway('users', $container->get(AdapterInterface::class), new RowGatewayFeature('id'));
        return new UsersModel($tableGateway);
    }
}