<?php

declare (strict_types = 1);

namespace App\Models\Factories;

use Psr\Container\ContainerInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use App\Models\UsersGroupsModel;
use Zend\Db\Adapter\AdapterInterface;

class UsersGroupsModelFactory
{

    public function __invoke(ContainerInterface $container): UsersGroupsModel
    {
        $tableGateway = new TableGateway('usersgroups', $container->get(AdapterInterface::class), new RowGatewayFeature('id'));
        return new UsersGroupsModel($tableGateway);
    }
}
