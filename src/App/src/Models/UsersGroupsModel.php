<?php

namespace App\Models;

use App\Entities\MemberGroupEntity;
use App\Entities\UsersGroupsEntity;
use DateTime;
use Zend\Db\TableGateway\TableGateway;
use App\Constantes\Format;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 *
 * @author lifting
 */
class UsersGroupsModel extends Model
{

    public function __construct(TableGateway $tableGateway)
    {
        parent::__construct($tableGateway);
        $this->entityClass = \App\Entities\usersGroupsEntity::class;
    }

    /**
     * Récupère les prochains passage des membres
     * @param int $idGroup 
     * @return array
     */
    public function getNextMembersGroup(int $idGroup): array
    {

        $sql = 'SELECT users.id, users.name, usersgroups.date_action, IFNULL(slt_nb.nb_passage,0) as nb_passage 
                FROM usersgroups 
                JOIN users ON users.id = usersgroups.id_user 
                LEFT JOIN(SELECT usersgroups.id_user, COUNT(usersgroups.date_action) as nb_passage
                    FROM usersgroups
                    WHERE usersgroups.date_action < NOW() AND usersgroups.id_group = :id
                    GROUP BY usersgroups.id_user) as slt_nb ON slt_nb.id_user = usersgroups.id_user
                WHERE DATE(usersgroups.date_action) >= DATE(NOW())
                    AND usersgroups.id_group = :id
                ORDER BY usersgroups.date_action';

        $groups = $this->queryString($sql, ['id' => $idGroup]);
        $members = [];

        foreach ($groups as $value) {
            $member = new MemberGroupEntity();
            $member->setIdGroup($idGroup)
                ->setIdUser((int)$value['id'])
                ->setName($value['name'])
                ->setDateAction($value['date_action'])
                ->setNb_passage((int)$value['nb_passage']);
            $members[] = $member;
        }

        return $members;
    }

    /**
     * Met à jour les membres déjà passés de tous les groupes
     * @return int|null nombre de membre mis à jour
     */
    public function updateMemberForNextDate(): ?int
    {
        //get group with member to update
        $sql = 'SELECT usersgroups.id_user, usersgroups.id_group, groups.period, MAX(date_action) AS date_action
                FROM usersgroups
                JOIN groups ON groups.id = usersgroups.id_group
                GROUP BY usersgroups.id_group, usersgroups.id_user
                HAVING DATE(date_action) < DATE(NOW())
                ORDER BY date_action';

        $groups = $this->queryString($sql);
        $nbGroups = count($groups);
        if ($nbGroups > 0) {

            foreach ($groups as $currentGroup) {

                //get last member date
                $lastMember = $this->getLastToPass($currentGroup['id_group']);
                if ($lastMember) {
                    $lastDate = new DateTime($lastMember->getDate_action());
                    $lastDate->modify('+1 day')
                        ->modify($currentGroup['period']);
                } else {
                    $lastDate = new DateTime($currentGroup['period']);
                }

                //récupérer la liste des membres du groupe à mettre à jour
                $sql = 'SELECT usersgroups.id_user, MAX(usersgroups.date_action) AS date_action
                        FROM usersgroups
                        WHERE usersgroups.id_group = :id
                        GROUP BY usersgroups.id_user
                        HAVING DATE(date_action) < DATE(NOW())';

                $members = $this->queryString($sql, ['id' => $currentGroup['id_group']]);

                foreach ($members as $currentMember) {
                    $member = new UsersGroupsEntity();
                    $member->setId_group($currentGroup['id_group'])
                        ->setId_user($currentMember['id_user'])
                        ->setDate_action($lastDate->format(Format::DATE_DB));

                    $this->add($member);

                    $lastDate->modify($currentGroup['period']);
                }
            }
        }

        return $nbGroups;
    }

    /**
     * Récupére le dernier member à passer
     * @param int $idGroup
     * @return UsersGroupsEntity|null
     */
    public function getLastToPass(int $idGroup): ?UsersGroupsEntity
    {
        $sql = 'SELECT id, id_user, id_group, max(date_action) as date_action '
            . 'FROM usersgroups '
            . 'WHERE id_group = :id '
            . 'AND DATE(date_action) >= DATE(NOW()) '
            . 'GROUP BY id_group';

        $groups = $this->queryString($sql, ['id' => $idGroup]);

        if (count($groups) > 0) {
            $userGroup = new UsersGroupsEntity();
            return $userGroup->exchangeArray($groups[0]);
        }

        return null;
    }
}
