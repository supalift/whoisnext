<?php

namespace App\Forms;

use Zend\Filter\StringTrim;
use Zend\InputFilter\InputFilter;
use Zend\Validator\EmailAddress;
use Zend\Validator\Hostname;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GroupCreate
 *
 * @author lifting
 */
class GroupAddMemberForm extends Form {

    public function __construct($name = 'groupAddMember', $options = []) {
        parent::__construct($name, $options);

        $this->add([
                    'name' => 'email',
                    'type' => 'email',
                ])
                ->add([
                    'name' => 'name',
                    'type' => 'text'
        ]);

        $this->setInputFilter($this->createFilter());
    }

    public function createFilter(): InputFilter {
        $inputFilter = new InputFilter();

        $inputFilter->add([
                    'name' => 'email',
                    'required' => true,
                    'filters' => [
                        ['name' => StringTrim::class],
                    ],
                    'validators' => [
                        ['name' => EmailAddress::class,
                            'options' => [
                                'allow' => Hostname::ALLOW_DNS,
                                'useMxCheck' => false,
                            ]
                        ]
                    ]
                ])
                ->add([
                    'name' => 'name',
                    'required' => true,
                    'filters' => [
                        ['name' => StringTrim::class],
                    ],
        ]);

        return $inputFilter;
    }

}
