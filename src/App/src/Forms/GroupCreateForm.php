<?php

namespace App\Forms;

use App\Entities\groupsEntity;
use App\Entities\usersEntity;
use Zend\Filter\StringTrim;
use Zend\InputFilter\InputFilter;
use Zend\Validator\EmailAddress;
use Zend\Validator\Hostname;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GroupCreate
 *
 * @author lifting
 */
class GroupCreateForm extends Form
{

    /*@var $user usersEntity */
    private $user;
    /*@var $group groupsEntity */
    private $group;

    public function __construct($name = 'groupCreate', $options = array())
    {
        parent::__construct($name, $options);

        $this->add([
            'name' => 'userEmail',
            'type' => 'email',
        ])
            ->add([
                'name' => 'name',
                'type' => 'text'
            ])
            ->add([
                'name' => 'groupName',
                'type' => 'text'
            ])
            ->add([
                'name' => 'period',
                'type' => 'text'
            ]);

        $this->setInputFilter($this->createFilter());
    }

    public function createFilter(): InputFilter
    {
        $inputFilter = new InputFilter();

        $inputFilter->add([
            'name' => 'userEmail',
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => EmailAddress::class,
                    'options' => [
                        'allow' => Hostname::ALLOW_DNS,
                        'useMxCheck' => false,
                    ]
                ]
            ]
        ])
            ->add([
                'name' => 'name',
                'required' => true,
                'filters' => [
                    ['name' => StringTrim::class],
                ],
            ])
            ->add([
                'name' => 'groupName',
                'required' => true,
                'filters' => [
                    ['name' => StringTrim::class],
                ],
            ])
            ->add([
                'name' => 'period',
                'required' => true,
                'filters' => [
                    ['name' => StringTrim::class],
                ],
            ]);

        return $inputFilter;
    }

    function getUserEntity(): usersEntity
    {
        if (!$this->user) {
            $this->user = new usersEntity();
        }

        $this->user->setEmail($this->get('userEmail')->getValue())
            ->setName($this->get('name')->getValue());

        return $this->user;
    }

    function getGroupEntity(): groupsEntity
    {
        if (!$this->group) {
            $this->group = new groupsEntity();
        }

        $this->group->setName($this->get('groupName')->getValue())
            ->setPeriod($this->get('period')->getValue());

        return $this->group;
    }
}
