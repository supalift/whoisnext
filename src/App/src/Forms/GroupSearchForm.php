<?php

namespace App\Forms;

use Zend\Filter\StringTrim;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GroupCreate
 *
 * @author lifting
 */
class GroupSearchForm extends Form {

    public function __construct($name = 'groupSearch', $options = []) {
        parent::__construct($name, $options);

        $this->add([
                    'name' => 'name',
                    'type' => 'text'
        ]);

        $this->setInputFilter($this->createFilter());
    }

    public function createFilter(): InputFilter {
        $inputFilter = new InputFilter();

        $inputFilter->add([
                    'name' => 'name',
                    'required' => true,
                    'filters' => [
                        ['name' => StringTrim::class],
                    ],
        ]);

        return $inputFilter;
    }

}
