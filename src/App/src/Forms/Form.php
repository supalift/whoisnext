<?php
declare (strict_types = 1);

namespace App\Forms;

use Zend\Form\Form as zendForm;

class Form extends zendForm {

    public function getErrorMessage():array{
        $errorMsg = [];

        foreach($this->getMessages() as $key => $message){
            $error = $key.'|'.key($message);
            $msg = ['type' => 'danger',
                    'message' => $error];
            
            $errorMsg['formMessages'][] = $msg;
        }

        return $errorMsg;
    }

}