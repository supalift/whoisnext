<?php

$container = require 'config/container.php';
$dbConfig = $container->get('config')['db'];

return [
    'paths' => [
        'migrations' => __DIR__ . '/db/migrations',
        'seeds' => __DIR__ . '/db/seeds'
    ],
    'environments' => [
        'default_database' => 'development',
        'development' => [
            'adapter' => 'mysql',
            'charset' => $dbConfig['charset'],
            'host' => $dbConfig['host'],
            'name' => $dbConfig['database'],
            'user' => $dbConfig['user'],
            'pass' => $dbConfig['password'],
            'port' => $dbConfig['port']
        ]
    ]
];
